Dante-server for
Telegram-proxy
Version 1.4.1-1

docker-compose.yml:

    version: '2'
    services:
        dante-server:
          image: divlex/telegram-proxy:1.4.1-1
          container_name: danted
          ports:
             - "1080:1080"
          volumes:
             - ./danted.conf:/etc/danted.conf
          environment:
             USER: 'user'
             PASSWORD: 'pass'
#----------------------------------------------------------------
docker-entrypoint.sh:

    #!/bin/bash
    useradd $USER
    echo -e "$PASSWORD\n$PASSWORD\n" |passwd $USER
    danted

#----------------------------------------------------------------
Dockerfile:

    FROM ubuntu:16.04
    MAINTAINER Divlex <alexey.roshchenko@lykkex.com>
    LABEL Description="Dante server"
    ENV GIT_RELEASE 1.4.1-1
    ENV GIT_REPO dante-server_${GIT_RELEASE}_amd64.deb
    ENV USERNAME user
    ENV PASSWORD pass

    RUN   apt-get update && apt-get install -y build-essential pkg-config net-tools \
          autoconf libtool unzip git python \
          wget curl vim automake dante-server gdebi-core
    WORKDIR /root
    RUN wget http://ppa.launchpad.net/dajhorn/dante/ubuntu/pool/main/d/dante/dante-server_1.4.1-1_amd64.deb && \
        pwd && ls -la && \
        dpkg -i ${GIT_REPO};

    ADD ./danted.conf /etc/danted.conf

    ADD docker-entrypoint.sh /
    RUN chmod +x /docker-entrypoint.sh
    ENTRYPOINT ["/docker-entrypoint.sh"]
    #CMD ["danted"]

#----------------------------------------------------------------
danted.conf:

    # /etc/danted.conf
    
    logoutput: /dev/stdout
    user.privileged: root
    user.unprivileged: nobody
    
    internal: 0.0.0.0 port=1080
    
    external: eth0
    
    socksmethod: username
    
    clientmethod: none
    
    client pass {
            from: 0.0.0.0/0 to: 0.0.0.0/0
            log: connect disconnect error
    }
    
    socks pass {
            from: 0.0.0.0/0 to: 0.0.0.0/0
            command: bind connect udpassociate
            log: connect disconnect error
    }
